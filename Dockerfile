FROM pombase/canto-base:v1
MAINTAINER Kim Rutherford <kim@pombase.org>

COPY . canto/

EXPOSE 7000
