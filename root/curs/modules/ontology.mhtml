<%args>
$annotation_namespace
$current_component_display_name
$current_component_short_display_name
$current_component_very_short_display_name
$current_component_suggest_term_help
$current_component
$broad_term_suggestions
$specific_term_examples
$annotation_help_text
$annotation_more_help_text
$annotation_extra_help_text
$curs_root_uri
$feature_type
$feature
$pub
</%args>

<div ng-controller="OntologyTermLocatorCtrl">

<div id="ferret">
    <div ng-hide="CursStateService.currentTerm()" id="ferret-term-entry" class="curs-box">

      <div class="inline-help curs-box-title">
Search for <% $current_component_display_name %> term
<& /curs/inline_help.mhtml, key => "${current_component}_definition" &>
      </div>
      <div class="ferret-term-entry-inside curs-box-body">
        <div>
          <% $annotation_help_text %>
% if (defined $annotation_more_help_text) {
          <a id="ferret-term-entry-type-help" class="canto-more-button" href='#'>more ...</a>
% }
        </div>
% if (defined $annotation_more_help_text) {
        <div id="ferret-term-entry-type-help-target" class="ferret-more-help">
          <% $annotation_more_help_text %>
        </div>
% }
        <div>
Start typing a <% $current_component_short_display_name %> in the search
box. If you do not find the term you are looking for with your initial search,
begin with a broad term (<% $broad_term_suggestions %>)
          <a id="ferret-term-entry-extra-help" class="canto-more-button" href='#'>more ...</a>
        </div>
        <div id="ferret-term-entry-extra-help-target" class="ferret-more-help">
          <div>
More specific terms will be suggested allowing you to refine your search
iteratively before making your final selection (examples of more specific
terms are: <% $specific_term_examples %>).
          </div>
          <div>
Gene products may be annotated with multiple
<% $current_component_short_display_name %> terms.
          </div>
% if (defined $annotation_extra_help_text) {
          <div>
<% $annotation_extra_help_text %>
          </div>
% }
        </div>
        <div id="ferret-term-entry-extra-help-target" class="ferret-more-help">
          <% $annotation_help_text %>
        </div>
        <term-name-complete id="ferret-term-input" name="ferret-term-entry"
                            current-term-name=""
                            found-callback="termFoundCallback(termId, termName, searchString, matchingSynonym)"
                            annotation-type-name="{{annotationTypeName}}"></term-name-complete>

        <div class="clearall">
          <a href='<% "$curs_root_uri/feature/$feature_type/view/$feature_id" %>'
             class="btn btn-primary curs-back-button">&lt;- Back</a>
        </div>
      </div>
    </div>

    <div ng-show="CursStateService.currentTerm() && !CursStateService.termConfirmed()" id="ferret-term-details" 
         class="curs-box ng-cloak">
      <div id="ferret-help-step-1" class="inline-help curs-box-title">
Please read the definition of the currently selected term to ensure that
it accurately describes your gene:
      </div>
      <div class="curs-box-body">
        <div id="ferret-linkouts" class="curs-external-links" style="display: none">
          <div class="links-title">
            View term ancestry in:
          </div>
          <div class="links-container">
          </div>
        </div>

        <table class="curs-definition-table">
          <tr>
            <td class="title">ID</td>
            <td class="term-id ferret-term-id-display">
              <span ng-hide="getCurrentTermDetails('id')">Loading ...</span>
              <span ng-show="getCurrentTermDetails('id')">{{getCurrentTermDetails('id')}}</span>
            </td>
          </tr>
          <tr>
            <td class="title">Ontology</td>
            <td><% $annotation_namespace %></td>
          </tr>
          <tr>
            <td class="title">Term name</td>
            <td class="term-name ferret-term-name">
              <span ng-hide="getCurrentTermDetails('name')">Loading ...</span>
              <span ng-show="getCurrentTermDetails('name')">{{getCurrentTermDetails('name')}}</span>
            </td>
          </tr>
          <tr>
            <td class="title">Definition</td>
            <td id="ferret-term-definition" class="term-definition">{{getCurrentTermDetails('definition')}}</td>
          </tr>
          <tr ng-show="CursStateService.matchingSynonym()">
            <td class="title">Matching synonym</td>
            <td>{{CursStateService.matchingSynonym()}}</td>
          </tr>
          <tr ng-show="getCurrentTermDetails('comment')">
            <td class="title">Comment</td>
            <td>{{getCurrentTermDetails('comment')}}</td>
          </tr>
          <tr ng-show="getCurrentTermDetails('synonyms')">
            <td class="title">Synonym<span ng-show="getCurrentTermDetails('synonyms').length != 1">s</span> </td>
            <td>
              <div ng-repeat="synonym in getCurrentTermDetails('synonyms')">
                {{synonym}}
              </div>
            </td>
          </tr>
        </table>
      </div>

      <div ng-show="getCurrentTermDetails('children')" id="ferret-term-children">
        <div id="ferret-help-step-2" class="inline-help curs-box-title">
If possible, please refine your term selection. Clicking on a child term
from the list below selects it and shows its definition (and children,
if any):
        </div>
        <div id="ferret-term-children-list" class="curs-box-body">
          <ul>
            <li ng-repeat="child in getCurrentTermDetails('children')">
              <a href="#" ng-click="goToChild(child.id)">{{child.name}}
                <img ng-src="{{CantoGlobals.application_root}}/static/images/right_arrow.png"></img>
              </a>
            </li>
          </ul>
        </div>

        <div class="inline-help curs-box-title">
          If you need a more specific term to describe the experiment you are
          annotating, and if none of terms above is appropriate, you can
          suggest a new term:
        </div>
        <div id="ferret-suggest-link" class="curs-box-body">
          <ul>
            <li>
              <a ng-click="openTermSuggestDialog('<% $feature_display_name %>')">
                Suggest a new child term for <span class="ferret-term-id-display">{{getCurrentTermDetails('id')}}</span> ...
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div ng-hide="getCurrentTermDetails('children')" id="ferret-leaf">
        <div id="ferret-help-step-2-suggest" class="inline-help curs-box-title">
The currently selected term has no children. If you need a more specific
term to describe the experiment you are annotating, please follow the
link below to suggest it:
          <a id="ferret-help-step-2-suggest-help" class="canto-more-button" href='#'>more ...</a>

          <div id="ferret-help-step-2-suggest-help-target" class="ferret-more-help">
            <div>
You may need a new term if the biology you are describing is not defined
by existing terms in a way that will distinguish it from related
<% $current_component_very_short_display_name %> terms. Sometimes this will be
a novel <% $current_component_short_display_name %> which has not been
previously described, and so is not yet included in the ontology.
            </div>
% if (defined $current_component_suggest_term_help) {
            <div>
<% $current_component_suggest_term_help %>
            </div>
% }
          </div>
        </div>
        <div id="ferret-suggest-link-leaf" class="curs-box-body">
          <ul>
            <li>
              <a ng-click="openTermSuggestDialog('<% $feature_display_name %>')">
Suggest a new child term for <span class="ferret-term-id-display">{{getCurrentTermDetails('id')}}</span> ...
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div id="ferret-accept-term-proceed-message">
        Otherwise, use the currently selected term:
        
        <button type="submit" class="btn btn-primary curs-finish-button"
                ng-click="confirmTerm()" >Proceed -></button>
      </div>

      <button type="submit" class="btn btn-primary curs-back-button"
                title="&lt;- Back"
                ng-click="unsetTerm()">&lt;- Back</button>
    </div>
</div>

<div ng-show="CursStateService.termConfirmed() && annotationType" class="ng-cloak">

  <div class="curs-box evidence-select">
    <div class="curs-box-title">
Choose evidence <span ng-show="annotationType.can_have_conditions">and conditions</span> for annotating <% $feature_display_name %> with {{data.term_ontid}}
    </div>
    <div class="curs-box-body">
      <annotation-evidence evidence-code="data.evidence_code"
                           conditions="data.conditions"
                           with-gene-id="data.with_gene_id"
                           valid-evidence="data.validEvidence"
                           annotation-type-name="{{annotationTypeName}}"></annotation-evidence>
    </div>
  </div>

<button type="submit" class="btn btn-primary curs-back-button"
        title="&lt;- Back"
        ng-click="unconfirmTerm()">&lt;- Back</button>
<button type="submit" class="btn btn-primary curs-finish-button"
        ng-disabled="!isValid()"
        ng-click="setTermAndEvidence()" >Proceed -&gt;</button>

</div>
</div>

<script language="javascript" type="text/javascript">
var ontology_external_links = <% $ontology_external_links_json |n %>;
</script>

<%init>
use JSON;

my $ontology_external_links_json =
  encode_json $c->config()->{ontology_external_links};

my $feature_display_name = $feature->display_name();
my $feature_id = $feature->feature_id();
</%init>
